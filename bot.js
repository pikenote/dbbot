/*
 * Discord Bot Maker Bot
 * Version 1.3.0
 * Robert Borghese
 */

const Files = require(require('path').join(process.cwd(), 'js', 'Main.js')).Files;

if(!process.send) {

Files.initStandalone();

} else {

process.on('message', function(content) {
	Files.initBotTest(content);
});

}